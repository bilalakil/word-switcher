Word Switcher
=============

[![word-switcher version](https://badge.fury.io/js/word-switcher.svg)](https://badge.fury.io/js/word-switcher)

A simple tool which, given an element and an array of "words" (or any HTML in general), switches between them in a loop and applies animation classes for you to work with. Bring your own CSS for animation/transitioning (or try our very simple examples).

Written in JavaScript (ES6).

## Usage

```
import wordSwitcher from 'word-switcher';
// Or just use `wordSwitcher` from the global scope if you're not using ES6 modules.

const target = document.querySelector('#target');
const words = ['and', 'then', 'there', 'were', 'none'];

wordSwitcher(target, words);
```

See [`index.html`][index.html] for example usage. Note that there it's being used as a module, which requires a modern browser. You can execute the demo via `npm install && npm run dev` (without any transpiling - use a modern browser with ES6 enabled).

### Installation

- __Best for Babel projects:__ Use `npm install --save word-switcher` to include into Node.js (ES6) projects via `import wordSwitcher from 'word-switcher'`. You may need to use a transpiler, or you could use one of the other installation options which have been transpiled already.
- __Best for non-Babel projects:__ Use `npm install --save word-switcher` and find the transpiled file for usage in the `dist` directory (i.e. `node__modules/word-switcher/dist/word-switcher.js`), and include it in your project however you wish. This puts the `wordSwitcher` function into the global scope for you to use.
- __Best for quick usage:__ Use the [unpkg CDN][unpkg]: `https://unpkg.com/word-switcher` points to the distribution file as in the previous installation method.

### Parameters

Configuration paramaters can be provided as the third argument to the `wordSwitcher` function in the form of an object. Choose your keys from below where you want to override the default values.

#### `switchDelay`

Default: `3000`

How long to wait before starting the switch to the next word? Note that this does not include animation/transition time. That is, if the animations took 1 second each, and the switch delay was 3 seconds, then it would be a total of 5 seconds in between two words.

#### `animationDuration`

Default: `0`

How long to allow for animations or transitions?

If `null` then instead of using a fixed time, the tool will wait for the `animationend` or `transitionend` events to trigger, so the timing will be derived from your CSS. Note that this may not be appropriate if multiple animations/transitions are being applied, as the first to complete will trigger the event. In that case you should specify an animation duration manually and that will be applied to the start and end.

If `0` then no animation will be performed.

#### `random`

Default: `false`

If `false` then the words will be switched in the order that they were received in the array. If `true` then the order in the array will be ignored and the next word to switch to will be selected randomly.

#### `className`

Default: `'word-switcher'`

The following classes are added to the "word" that is switching:

- `word-switcher` (all the time)
- `word-switcher-enter`
- `word-switcher-enter-to`
- `word-switcher-enter-active`
- `word-switcher-leave`
- `word-switcher-leave-to`
- `word-switcher-leave-active`

You can change the prefix (and the first `word-switcher` class) by overriding the default `className` value. See the following section regarding how to use the `enter*` and `leave*` classes...

### Transitions/Animation

By default there is no animation, which you can see via the default `animationDuration` of `0`. You can easily provide your own animation setting `animationDuration` a value other than `0` (see the `animationDuration` description for more information on that), and then taking advantage of the `enter*` and `leave*` classes that are applied to the containers of the switching words.

Usage of those animation classes were inspired from the awesome Vue.js and I've done my best to imitate its functionality, so you'd be well off reading Vue's [Transition Classes][vue-transition-classes] section and the two following ([CSS Transitions](vue-css-transitions) and [CSS Animations](vue-css-animations)) it.

Given the default `className` of `word-switcher`, here's a minimal CSS transition example which will fade the words in and out:

```
.word-switcher-enter-active, .word-switcher-leave-active {
  transition: opacity 1s
}
.word-switcher-enter, .word-switcher-leave-to {
  opacity: 0
}
```

Do share any cool animations or transitions that you come up with!

[vue-transition-classes]: https://vuejs.org/v2/guide/transitions.html#Transition-Classes
[vue-css-transitions]: https://vuejs.org/v2/guide/transitions.html#CSS-Transitions
[vue-css-animations]: https://vuejs.org/v2/guide/transitions.html#CSS-Animations
[index.html]: https://bitbucket.org/bilalakil/word-switcher/src/master/index.html?at=master
[unpkg]: https://unpkg.com/
